#!/bin/bash

declare -a perspectives=("iso" "side")
num_perspectives=${#perspectives[@]}

declare -a animations=("walk")
num_animations=${#animations[@]}

for (( p=0; p<${num_perspectives}; p++ ));
do
    last_direction=15

    if [ $p -eq 1 ]; then
        last_direction=3
    fi

    for (( a=0; a<${num_animations}; a++ ));
    do
        ani=render/${perspectives[$p]}/${animations[$a]}
        frames=8

        for i in {0..15}
        do
            if [ $i -gt $last_direction ]; then
                break
            fi

            start=$((i*frames))
            ffmpeg -y -framerate 12 -start_number $start -i $ani/%03d.png -plays 0 -frames:v $frames $ani$i.apng
        done
    done
done
