#!/bin/bash

declare -a perspectives=("iso" "side")
num_perspectives=${#perspectives[@]}

declare -a animations=("walk")
num_animations=${#animations[@]}

for (( p=0; p<${num_perspectives}; p++ ));
do
    last_sheet=7
    d=8
    delta=64

    if [ $p -eq 1 ]; then
        last_sheet=1
        d=16
        delta=8
    fi

    for (( a=0; a<${num_animations}; a++ ));
    do
        ani=render/${perspectives[$p]}/${animations[$a]}
        for i in {0..7}
        do
            if [ $i -gt $last_sheet ]; then
                break
            fi

            first=$((i*d));
            firstlast=$((first+7));
            second=$((first+delta));
            secondlast=$((second+7))

            montage \
            <(montage -mode concatenate -background transparent -tile 4x ''"$ani"'/%03d.png'["$first"-"$firstlast"] png:-) \
            <(montage -mode concatenate -background transparent -tile 4x ''"$ani"'/%03d.png'["$second"-"$secondlast"] png:-) \
            -mode concatenate -background transparent -tile 1x2 $ani$i.png
        done
    done
done
