import bpy
import os

scn = bpy.data.scenes['Portraits']
bpy.context.screen.scene = scn
output_path = scn.render.filepath

# iterate through markers and render
for k, m in scn.timeline_markers.items():
    frame = m.frame
    scn.frame_set(frame)
    scn.render.filepath = os.path.join(output_path, m.name.lower() + ".png")
    bpy.ops.render.render(write_still=True)

bpy.context.scene.render.filepath = output_path
