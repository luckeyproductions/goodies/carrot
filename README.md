# [![](https://licensebuttons.net/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/) Carrot _in 3D_

Model and animations by Frode 魔大农 Lindeijer, original character design by [David Revoy](https://www.peppercarrot.com/).


### Render artifacts

- [**Isometric**](https://gitlab.com/luckeyproductions/goodies/carrot/-/jobs/artifacts/main/download?job=isometric)
- [**Side-scroller**](https://gitlab.com/luckeyproductions/goodies/carrot/-/jobs/artifacts/main/download?job=side-scroller)
- [**Portraits**](https://gitlab.com/luckeyproductions/goodies/carrot/-/jobs/artifacts/main/download?job=portraits)


 ![](portraits/neutral.png) | ![](portraits/beg.png)
---|---
![](portraits/cross.png) | ![](portraits/surprized.png)
